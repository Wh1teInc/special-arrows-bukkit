package occupation.littlewhite.specialarrows.arrows;

import occupation.littlewhite.specialarrows.utility.StringPath;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.persistence.PersistentDataContainer;
import org.bukkit.persistence.PersistentDataType;

public class Arrows {
    public Arrows(Player player) {
        this.player = player;
    }

    private final Player player;

    private ItemStack arrow = null;
    private Types types = null;

    /**
     * Делаем проверку на предмет, который должен находится в руке,
     * в данном случае - это стрела
     */
    private void setArrow() {
        ItemStack itemInOffHand = player.getInventory().getItemInOffHand();
        if (itemInOffHand.getType() != Material.ARROW) return;
        arrow = itemInOffHand;
        initializeArrow();
    }

    /**
     * Инициализируем стрелу, ее тип
     */
    private void initializeArrow() {
        ItemMeta im = arrow.getItemMeta();
        assert im != null;
        PersistentDataContainer dataContainer = im.getPersistentDataContainer();
        if (dataContainer.has(StringPath.EFFECT_KEY, PersistentDataType.INTEGER)) {
            Integer key = dataContainer.get(StringPath.EFFECT_KEY, PersistentDataType.INTEGER);

            if (key == null) return;
            switch (key) {
                case 0 -> types = Types.EXPLOSIVE;
                case 1 -> types = Types.LIGHTNING;
                case 2 -> types = Types.TELEPORT;
                case 3 -> types = Types.RIDE;
                case 4 -> types = Types.GRAVITY;
                case 5 -> types = Types.BUDLE_ON;
                case 6 -> types = Types.DIAMOND;
                case 7 -> types = Types.END_CRYSTAL;
                case 8 -> types = Types.FISH;
                case 9 -> types = Types.INFINITY;
            }
        }
    }

    /**
     * Получаем тип стрелы
     * @param arrow
     * @return
     */
    public Types getTypes(ItemStack arrow) {
        ItemMeta im = arrow.getItemMeta();
        assert im != null;
        PersistentDataContainer dataContainer = im.getPersistentDataContainer();
        if (dataContainer.has(StringPath.EFFECT_KEY, PersistentDataType.INTEGER)) {
            Integer key = dataContainer.get(StringPath.EFFECT_KEY, PersistentDataType.INTEGER);

            if (key == null) return null;
            switch (key) {
                case 0 -> types = Types.EXPLOSIVE;
                case 1 -> types = Types.LIGHTNING;
                case 2 -> types = Types.TELEPORT;
                case 3 -> types = Types.RIDE;
                case 4 -> types = Types.GRAVITY;
                case 5 -> types = Types.BUDLE_ON;
                case 6 -> types = Types.DIAMOND;
                case 7 -> types = Types.END_CRYSTAL;
                case 8 -> types = Types.FISH;
                case 9 -> types = Types.INFINITY;
        }
    }
        return types;
}

    /**
     * Получаем тип стрелы, если найден, то все ок
     */
    public Types getTypes() {
        return types;
    }

    /**
     * Получаем стрелу, если найдена, то возвращаем ее же
     */
    public ItemStack getArrow() {
        return arrow;
    }
}
