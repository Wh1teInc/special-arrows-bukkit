package occupation.littlewhite.specialarrows.arrows;

import org.bukkit.Material;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.persistence.PersistentDataContainer;

public class ArrowFactory {
    public ItemStack createArrow(Types types) {
        ItemStack arrow = new ItemStack(Material.ARROW);
        arrow.addUnsafeEnchantment(Enchantment.DURABILITY, 0);
        ItemMeta im = arrow.getItemMeta();
        assert im != null;
        PersistentDataContainer dataContainer = im.getPersistentDataContainer();

        int key;

        return arrow;
    }
    public ItemStack createArrow(Types types, int amount) {
        ItemStack arrow = new ItemStack(Material.ARROW);
        arrow.addUnsafeEnchantment(Enchantment.DURABILITY, 0);
        ItemMeta im = arrow.getItemMeta();
        assert im != null;
        PersistentDataContainer dataContainer = im.getPersistentDataContainer();

        int key;
        // пока так
        return arrow;
    }
}
