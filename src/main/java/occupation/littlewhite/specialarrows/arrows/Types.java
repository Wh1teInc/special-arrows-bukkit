package occupation.littlewhite.specialarrows.arrows;

public enum Types {
    // todo: используются еще не все типы, но позже обязательно будут
    EXPLOSIVE,
    LIGHTNING,
    TELEPORT,
    RIDE,
    GRAVITY,
    BUDLE_ON,
    DIAMOND,
    END_CRYSTAL,
    FISH,
    INFINITY
}
