package occupation.littlewhite.specialarrows.crafting;

import occupation.littlewhite.specialarrows.arrows.ArrowFactory;
import occupation.littlewhite.specialarrows.arrows.Types;
import occupation.littlewhite.specialarrows.utility.ChatColorUtil;
import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.ShapedRecipe;
import org.bukkit.inventory.ShapelessRecipe;

public class ArrowCraftRecipe {
    public ArrowCraftRecipe() {
        bundleOfArrow();
        explosiveArrow();
        teleportArrow();
        rideArrow();
    }
    private final ArrowFactory arrowFactory = new ArrowFactory();

    private void bundleOfArrow() {
        ItemStack bundleOfArrow = arrowFactory.createArrow(Types.BUDLE_ON, 32);

        ShapedRecipe recipe = new ShapedRecipe(ChatColorUtil.getKey("BundleOfArrow"), bundleOfArrow);
        recipe.shape("xxx", "xxx", "xxx");
        recipe.setIngredient('x', Material.ARROW);

        Bukkit.addRecipe(recipe);
    }
    private void explosiveArrow() {
        ItemStack explosiveArrow = arrowFactory.createArrow(Types.EXPLOSIVE, 16);

        ShapedRecipe recipe = new ShapedRecipe(ChatColorUtil.getKey("ExplosiveArrow"), explosiveArrow);
        recipe.shape("xxx", "xyx", "xxx");

        recipe.setIngredient('x', Material.TNT);
        recipe.setIngredient('y', Material.ARROW);

        Bukkit.addRecipe(recipe);
    }
    private void teleportArrow() {
        ItemStack teleportArrow = arrowFactory.createArrow(Types.TELEPORT, 32);

        ShapedRecipe recipe = new ShapedRecipe(ChatColorUtil.getKey("TeleportArrow"), teleportArrow);
        recipe.shape("xxx", "xyx", "xxx");

        recipe.setIngredient('x', Material.ENDER_PEARL);
        recipe.setIngredient('x', Material.ARROW);
    }
    private void rideArrow() {
        ItemStack rideArrow = arrowFactory.createArrow(Types.RIDE, 16);

        ShapelessRecipe recipe = new ShapelessRecipe(ChatColorUtil.getKey("RideArrow"), rideArrow);
        recipe.addIngredient(Material.SADDLE);
        recipe.addIngredient(Material.ARROW);

        Bukkit.addRecipe(recipe);
    }
}
