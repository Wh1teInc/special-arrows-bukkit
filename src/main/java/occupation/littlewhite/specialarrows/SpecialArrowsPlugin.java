package occupation.littlewhite.specialarrows;

import occupation.littlewhite.specialarrows.commands.CommandCompleter;
import occupation.littlewhite.specialarrows.commands.SpecialArrowsCommand;
import occupation.littlewhite.specialarrows.crafting.ArrowCraftRecipe;
import occupation.littlewhite.specialarrows.utility.EventHandlers;
import org.bukkit.Bukkit;
import org.bukkit.plugin.java.JavaPlugin;

import java.util.Objects;

public class SpecialArrowsPlugin extends JavaPlugin {
    private static SpecialArrowsPlugin INSTANCE;

    @Override
    public void onEnable() {
        INSTANCE = this;
        new EventHandlers(Bukkit.getPluginManager(), this);
        Objects.requireNonNull(this.getCommand("specialarrow")).setExecutor(new SpecialArrowsCommand());
        Objects.requireNonNull(this.getCommand("specialarrow")).setTabCompleter(new CommandCompleter());
        new ArrowCraftRecipe();
    }

    @Override
    public void onDisable() {

    }

    public static SpecialArrowsPlugin getInstance() {
        return INSTANCE;
    }
}
