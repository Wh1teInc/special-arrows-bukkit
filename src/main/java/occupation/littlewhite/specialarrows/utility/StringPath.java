package occupation.littlewhite.specialarrows.utility;

import occupation.littlewhite.specialarrows.SpecialArrowsPlugin;
import org.bukkit.NamespacedKey;

public class StringPath {
    public static final NamespacedKey EFFECT_KEY = new NamespacedKey(SpecialArrowsPlugin.getInstance(), "ArrowEffects");
    public static final NamespacedKey SPECIAL_ARROW_RECIPE = new NamespacedKey(SpecialArrowsPlugin.getInstance(), "SpecialArrowRecipe");
}