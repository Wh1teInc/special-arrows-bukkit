package occupation.littlewhite.specialarrows.utility;

import occupation.littlewhite.specialarrows.SpecialArrowsPlugin;
import org.bukkit.ChatColor;
import org.bukkit.NamespacedKey;

public class ChatColorUtil {
    public static String color(String message) {
        return ChatColor.translateAlternateColorCodes('&', message);
    }
    public static NamespacedKey getKey(String key) {
        return new NamespacedKey(SpecialArrowsPlugin.getInstance(), key);
    }
}
