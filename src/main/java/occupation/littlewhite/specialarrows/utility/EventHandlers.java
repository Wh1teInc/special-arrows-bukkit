package occupation.littlewhite.specialarrows.utility;

import occupation.littlewhite.specialarrows.ResourcePackListener;
import occupation.littlewhite.specialarrows.SpecialArrowsPlugin;
import occupation.littlewhite.specialarrows.arrows.ArrowEvent;
import org.bukkit.plugin.PluginManager;

public class EventHandlers {
    public EventHandlers(PluginManager pm, SpecialArrowsPlugin INSTANCE) {
        pm.registerEvents(new ArrowEvent(), INSTANCE);
        pm.registerEvents(new ResourcePackListener(), INSTANCE);
    }
}
