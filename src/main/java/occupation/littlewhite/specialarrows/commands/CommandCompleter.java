package occupation.littlewhite.specialarrows.commands;

import occupation.littlewhite.specialarrows.arrows.Types;
import occupation.littlewhite.specialarrows.utility.ChatColorUtil;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.command.TabCompleter;
import org.bukkit.entity.Player;

import java.util.ArrayList;
import java.util.List;

public class CommandCompleter implements TabCompleter {
    @Override
    public List<String> onTabComplete(CommandSender sender, Command command, String label, String[] args) {
        if (!(sender instanceof Player player)) return null;
        if (!player.isOp()) return null;

        if (args.length == 1) {
            return types();
        }

        if (args.length == 2) {
            List<String> amount = new ArrayList<>();
            amount.add(ChatColorUtil.color("&8[Количество]"));
            return amount;
        }
        return null;
    }
    public List<String> types() {
        List<String> types = new ArrayList<>();

        for (Types type : Types.values()) {
            types.add(type.name());
        }
        return types;
    }
}