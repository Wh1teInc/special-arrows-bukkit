package occupation.littlewhite.specialarrows.commands;

import occupation.littlewhite.specialarrows.SpecialArrowsPlugin;
import occupation.littlewhite.specialarrows.arrows.ArrowFactory;
import occupation.littlewhite.specialarrows.arrows.Types;
import occupation.littlewhite.specialarrows.utility.ChatColorUtil;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class SpecialArrowsCommand implements CommandExecutor {
    @Override
    public boolean onCommand(CommandSender sender, Command command, String s, String[] args) {

        if (!(sender instanceof Player player)) {
            SpecialArrowsPlugin.getInstance().getLogger().warning(ChatColorUtil.color("&cOnly for players!!!"));
            return false;
        }
        if (!player.isOp()) {
            player.sendMessage(ChatColorUtil.color("&fОшибка! &cНедостаточно прав для выполнения!"));
            return false;
        }
        ArrowFactory arrowFactory = new ArrowFactory();

        if (args.length == 1) {
            String type = args[0];
            for (Types types : Types.values()) {
                if (types.name().equalsIgnoreCase(type)) {
                    player.getInventory().addItem(arrowFactory.createArrow(types));
                }
            }
        }
        else if (args.length == 2) {
            String type = args[0];
            int amount;
            try {
                amount = Integer.parseInt(args[1]);
            } catch (NumberFormatException e) {
                player.sendMessage("&cПожалуйста, укажите верное значение, а не блять 12938123982483289849932848932894");
                return false;
            }
            for (Types types : Types.values()) {
                if (types.name().equalsIgnoreCase(type)) {
                    player.getInventory().addItem(arrowFactory.createArrow(types, amount));
                }
            }
        } else {
            player.sendMessage(ChatColorUtil.color("&cИспользуйте: &e/specialarrow <тип> <кол-во>"));
        }
        return false;
    }
}
